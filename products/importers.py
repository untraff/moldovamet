from django.db import transaction
from .models import Category, Product
from django.conf import settings
import os
import json


def data_from_string(string: str, delimiter=','):
    return string.strip().split(delimiter)


def create_category_from_file():
    filename = os.path.join(settings.BASE_DIR, 'loading_data/categories/category_for_import2.csv')

    with open(filename, 'r', encoding='utf8') as category_data:

        new_categories = []

        for row in category_data.readlines():
            dr = data_from_string(row)
            id, name, slug, is_trash, parent_category = dr[0], dr[1], dr[2], 0, dr[4]
            id = int(id)
            try:
                parent_category = int(parent_category)
            except ValueError:
                if parent_category == '':
                    parent_category = None
            new_categories.append(Category(id=id, name=name, slug=slug, is_trash=is_trash,
                                           parent_category_id=parent_category))
        Category.objects.bulk_create(new_categories)


def import_products():
    DATA_FOLDER = os.path.join(settings.BASE_DIR, 'loading_data/products')

    file_names = [file for file in os.walk(DATA_FOLDER, False)][0][2]
    bad_categories = set()
    with transaction.atomic():
        for file_name in file_names:
            category_slug = file_name.split('__')[0]
            try:
                current_category = Category.objects.get(slug=category_slug)
            except Category.DoesNotExist:
                print(category_slug)
                raise Category.DoesNotExist()

            with open(f'{DATA_FOLDER}/{file_name}', 'r', encoding='windows-1251') as category_data:
                data_header = data_from_string(category_data.readline(), delimiter=';')
                products_for_insert = []
                for row in category_data.readlines()[1:]:
                    data_row = data_from_string(row, delimiter=';')
                    if len(data_row) != len(data_header) and category_slug != 'all.csv':
                        bad_categories.add(category_slug)
                    tmp = {}
                    for i in range(0, len(data_row)):
                        if i >= len(data_header):
                            tmp[data_header[-1]] += data_row[i]
                            continue
                        tmp[data_header[i]] = data_row[i]

                    json_data = json.dumps(tmp, ensure_ascii=False)
                    products_for_insert.append(
                        Product(category=current_category, price=0, specifications=json_data)
                    )
                Product.objects.bulk_create(products_for_insert)

    print(bad_categories)