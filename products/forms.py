from django import forms
import re

from django.core.exceptions import ValidationError
from django.core.mail import send_mail

from core.settings import EMAIL_HOST_USER


class PhoneField(forms.CharField):
    def validate(self, value):
        digits = re.findall("(\d)", value)
        if len(digits) < 7 or len(digits) > 11:
            raise ValidationError(
                'Некорректный номер: %(value)s введите от 7 до 11 цифр',
                params={'value': "".join(digits)},
            )

        super(PhoneField, self).validate(value)


class ContactForm(forms.Form):
    name = forms.CharField(
        label='Ваше имя',
        max_length=100,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': "Ваше имя",
            }
        )
    )
    phone = PhoneField(
        label='Ваше имя',
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'placeholder': "Ваш номер телефона",
            }
        )
    )

    def send_email(self):
        subj = f'Заявка от {self.cleaned_data["name"]} {self.cleaned_data["phone"]}'
        html = "\n".join([f"{k}: {v}" for k, v in self.cleaned_data.items()])
        send_mail(
            subj,
            html,
            EMAIL_HOST_USER,
            [EMAIL_HOST_USER,],
            fail_silently=False,
        )
        return

