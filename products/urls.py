from django.urls import path, include
from django.views.decorators.cache import cache_page


from . import views
from .views import CatgoryListView, ContactFormView

urlpatterns = [
    path('', views.IndexPage.as_view(), name='IndexPage'),
    path(
        'metall/<str:slug>/',
        # cache_page(60*15)(
        views.CategoryDetailView.as_view(),
        # ),
        name='CategoryPage'
    ),
    path('product/<int:pk>/', views.ProductDetailView.as_view(), name='ProductDetailPage'),
    path("order/", views.ContactFormView.as_view(), name="OrderPage"),
    path("thanks/", views.ThanksView.as_view(), name="ThanksPage")
]
