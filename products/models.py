from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse
import json
import pymorphy2

from products.specifications_lib import lib


ATTRIBUTE_MAPPER_TO_ENG = {'D': 'd', 'D внутренний': 'd-vnutrennij', 'D наружный': 'd-naruzhnyj', 'D трубы': 'd-truby', 'Dвн': 'd-vn', 'H': 'h', 'd': 'd', 'marka': 'marka', 'А': 'a', 'В': 'v', 'Вн.диаметр': 'vn.diametr', 'Высота': 'vysota', 'ГОСТ, ОСТ, ТУ': 'gost-ost-tu', 'Глубина': 'glubina', 'Диаметр': 'diametr', 'Диаметр проволоки, мм': 'diametr-provoloki,-mm', 'Диаметр, мм': 'diametr', 'Длина': 'dlina', 'Длина, м': 'dlina', 'Длинна': 'dlinna', 'Живое сечение сетки, %': 'zhivoe-sechenie-setki', 'Кол-во полос': 'kol-vo-polos', 'Марка': 'marka', 'Нар.диаметр': 'nar.diametr', 'Наруж. D изол.': 'naruzh.-d-izol.', 'Объем': 'obem', 'Описание': 'opisanie', 'Перемычка': 'peremychka', 'Р1': 'r1', 'Р2': 'r2', 'Размер': 'razmer', 'Размер 1': 'razmer-1', 'Размер 1, мм': 'razmer-1', 'Размер 2': 'razmer-2', 'Размер 2, мм': 'razmer-2', 'Размер 3': 'razmer-3', 'Размер 3, мм': 'razmer-3', 'Размер рамы 1, мм': 'razmer-ramy-1', 'Размер рамы 2, мм': 'razmer-ramy-2', 'Размер рамы 3, мм': 'razmer-ramy-3', 'Расстояние': 'rasstoyanie', 'Сторона ячейки, мм': 'storona-yachejki', 'Тип сплава': 'tip-splava', 'Типоразмер': 'tiporazmer', 'Толщина': 'tolshhina', 'Толщина стенки': 'tolshhina-stenki', 'Толщина, мкм': 'tolshhina', 'Толщина, мм': 'tolshhina', 'Фракция': 'frakciya', 'Фракция, мм': 'frakciya', 'Шаг свивки': 'shag-svivki', 'Ширина': 'shirina', 'Ширина 2-й полки': 'shirina-2-j-polki', 'Ширина полки': 'shirina-polki', 'Ширина, мм': 'shirina'}
ATTRIBUTE_MAPPER_TO_RUS = {'d': 'd', 'd-vnutrennij': 'D внутренний', 'd-naruzhnyj': 'D наружный', 'd-truby': 'D трубы', 'd-vn': 'Dвн', 'h': 'H', 'marka': 'Марка', 'a': 'А', 'v': 'В', 'vn.diametr': 'Вн.диаметр', 'vysota': 'Высота', 'gost-ost-tu': 'ГОСТ, ОСТ, ТУ', 'glubina': 'Глубина', 'diametr': 'Диаметр, мм', 'diametr-provoloki,-mm': 'Диаметр проволоки, мм', 'dlina': 'Длина, м', 'dlinna': 'Длинна', 'zhivoe-sechenie-setki': 'Живое сечение сетки, %', 'kol-vo-polos': 'Кол-во полос', 'nar.diametr': 'Нар.диаметр', 'naruzh.-d-izol.': 'Наруж. D изол.', 'obem': 'Объем', 'opisanie': 'Описание', 'peremychka': 'Перемычка', 'r1': 'Р1', 'r2': 'Р2', 'razmer': 'Размер', 'razmer-1': 'Размер 1, мм', 'razmer-2': 'Размер 2, мм', 'razmer-3': 'Размер 3, мм', 'razmer-ramy-1': 'Размер рамы 1, мм', 'razmer-ramy-2': 'Размер рамы 2, мм', 'razmer-ramy-3': 'Размер рамы 3, мм', 'rasstoyanie': 'Расстояние', 'storona-yachejki': 'Сторона ячейки, мм', 'tip-splava': 'Тип сплава', 'tiporazmer': 'Типоразмер', 'tolshhina': 'Толщина, мм', 'tolshhina-stenki': 'Толщина стенки', 'frakciya': 'Фракция, мм', 'shag-svivki': 'Шаг свивки', 'shirina': 'Ширина, мм', 'shirina-2-j-polki': 'Ширина 2-й полки', 'shirina-polki': 'Ширина полки'}

class CyrillicJSONField(models.JSONField):
    pass
    # def get_prep_value(self, value):
    #     if value is None:
    #         return value
    #     return json.dumps(value, cls=self.encoder, ensure_ascii=False)
    #
    # def validate(self, value, model_instance):
    #     super().validate(value, model_instance)
    #     try:
    #         json.dumps(value, cls=self.encoder, ensure_ascii=False)
    #     except TypeError:
    #         raise ValidationError(
    #             self.error_messages['invalid'],
    #             code='invalid',
    #             params={'value': value},
    #         )


class Product(models.Model):
    name = models.CharField(max_length=255, null=True, blank=True)
    price = models.IntegerField()
    specifications = CyrillicJSONField()
    category = models.ForeignKey('Category', models.SET_NULL, null=True, default=None, blank=True)
    description = models.TextField(null=True)
    seo_text = models.TextField(null=True)

    def __str__(self):
        return json.dumps(self.specifications, ensure_ascii=False)

    class Meta:
        ordering = ['-name']

    def get_absolute_url(self):
        return reverse("ProductDetailPage", kwargs={"pk": self.pk})

    def get_specification(self):
        return self.specifications.values()

    def get_headers_specification(self):
        return [ATTRIBUTE_MAPPER_TO_RUS.get(k) for k in self.specifications.keys()]

    @property
    def pretty_name(self):
        return ' '.join([v for v in self.specifications.values()])

    @property
    def price_per_meter(self) -> int:
        meters_per_ton = {
            6: 4506,
            8: 2532,
            10: 1620,
            12: 1126,
            14: 826,
            16: 633,
            18: 500,
            20: 405,
            22: 335,
            25: 260,
            28: 207,
            32: 158,
            36: 125,
            40: 101
        }
        size = int(self.specifications.get("razmer"))
        return round(self.price / meters_per_ton.get(size))

    def price_per_prut(self) -> int:
        try:
            length = float(self.specifications.get("dlina", "").replace(",", "."))
        except ValueError:
            return
        return int(self.price_per_meter * length)

    @property
    def text(self):

        CASES_CHOISES = {
            'femn': 'accs',
            'masc': 'nomn',
            'neut': 'accs',
        }

        res = []
        for key, val in json.loads(self.specifications).items():
            tmplt = lib.get(key, None)
            from .myre import text_generator
            tmplt = text_generator(tmplt)
            if tmplt:
                if key == 'Вид металла':
                    morph = pymorphy2.MorphAnalyzer()
                    words = val.split(' ')
                    words = [word.strip().lower() for word in words]
                    pm_words = [morph.parse(word) for word in words]
                    # TODO: мужской род, женский род, существительное
                    gender = None
                    padej = 'accs'
                    for word in enumerate(pm_words):
                        if word[1][0].tag.POS == 'NOUN':  # Если существительное меняем падеж в зависимости от Рода
                            padej = CASES_CHOISES.get(word[1][0].tag.gender)
                            words[word[0]] = word[1][0].inflect({padej, 'sing'}).word
                            break
                    # words = [word[0].inflect({padej, 'sing'}) for word in pm_words]
                    # res.append(tmplt.format(' '.join([word.word for word in words if word is not None])))
                    res.append(tmplt.format(' '.join(words)))
                    continue
                res.append(tmplt.format(val.strip().lower()))
        return '. '.join(res)


class Category(models.Model):
    name = models.CharField(max_length=255)
    slug = models.SlugField(blank=True, null=True)
    parent_category = models.ForeignKey('Category', on_delete=models.SET_NULL, null=True, blank=True)
    is_trash = models.BooleanField(default=False)
    seo_text = models.TextField(null=True, default="")

    def __str__(self):
        return self.name

    def get_highest_price(self):
        try:
            hightes_price = self.product_set.all().order_by('-price').first().price
        except AttributeError:
            hightes_price = 0
        return hightes_price

    def get_cheapest_price(self):
        try:
            cheapest_price = self.product_set.all().order_by('price').first().price
        except AttributeError:
            cheapest_price = 0
        return cheapest_price

    def get_parents(self, reverse=False) -> list:
        parents = []
        current_category = self
        while current_category.parent_category:
            parents.append(current_category.parent_category)
            current_category = current_category.parent_category
        if reverse:
            return parents[::-1]
        return parents

    def _category_child_handler(self, cat):
        result = Category.objects.none()
        if cat.category_set.exists():
            child_categoies = cat.category_set.prefetch_related('category_set').all()
            result = result.union(child_categoies)
            for category in cat.category_set.all():
                comeon = self._category_child_handler(category)
                if comeon.exists():
                    result = result.union(comeon)
        return result

    @property
    def childs(self):
        if self.parent_category is None:
            return Category.objects.none()
        return self._category_child_handler(self)

    @property
    def length(self):
        return len(self.name)

    def get_steel_grades(self):
        grades = {}
        for product in self.product_set.all():
            specifications = product.specifications
            if specifications:
                specs = specifications
                grade = specs.get('Марка', None)
                if grade is not None:
                    valid_grade = grade.strip()
                    grades.setdefault(valid_grade, 0)
                    grades[valid_grade] += 1
        return [g[0] for g in sorted(grades.items(), key=lambda x: x[1], reverse=True)]

    def get_gosts(self):
        grades = {}
        for product in self.product_set.all():
            specifications = product.specifications
            if specifications:
                specs = specifications
                grade = specs.get('ГОСТ, ОСТ, ТУ', None)
                if grade is not None:
                    valid_grade = grade.strip()
                    grades.setdefault(valid_grade, 0)
                    grades[valid_grade] += 1
        return [g[0] for g in sorted(grades.items(), key=lambda x: x[1], reverse=True)]

    def get_absolute_url(self):
        return reverse("CategoryPage", kwargs={"slug": self.slug})

    def get_filters(self, current_filters: dict):
        filters = {}
        selected_values = {k: {"selected": v} for k, v in current_filters.items() if not v.startswith("Выберите")}
        for product in self.product_set.all():
            for key, val in product.specifications.items():
                filters.setdefault(key, {
                    "values": set(),
                    "rus": ATTRIBUTE_MAPPER_TO_RUS.get(key, key),
                    "eng": key,
                })
                filters[key]["values"].add(val)
        for _, fil in filters.items():
            fil["values"] = sorted(fil["values"])
        [filters[x].update(selected_values[x]) for x in selected_values]
        return filters
