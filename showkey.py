"""
Проходится по всем спецификациями товаров, оставляет ключи. выводит еще 3 примера товаров с данным ключом
"""

import django
django.setup()

from products.models import Category
from django.urls import reverse
import json

res = {}

for category in Category.objects.all():
    for product in category.product_set.all():
        for spec_key in product.specifications.keys():
            res.setdefault(spec_key, {'counter': 0, 'examples': []})
            res[spec_key]['counter'] += 1
            if len(res[spec_key]['examples']) < 3:
                url = reverse("ProductDetailPage", kwargs={"pk": product.pk})
                res[spec_key]['examples'].append(url)
for row in sorted(res.items(), key=lambda x: x[1]['counter'], reverse=True):
    print(row[0], row[1]['counter'], '\t'.join(row[1]['examples']), sep='\t')

