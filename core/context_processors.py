from custompages.models import Page
from products.forms import ContactForm

COMPANY_NAME = 'Чермет прокат'
DOMAIN = 'pg-moscow.ru'


class MainPageMenuItem:
    slug = '/'
    name = 'Главная'


MENU = [p for p in Page.objects.all()]


DATA_LIB = {
    'COMPANY_NAME': COMPANY_NAME,
    'PHONE': '<a class="phone" href="tel:8 495 649-69-77"><nobr>8 495 649-69-77</nobr></a>',
    'PHONE_SHORT': '8 495 649-69-77',
    'EMAIL': f'<a class="email" href="mailto:mncmos@yandex.ru">mncmos@yandex.ru</a>',
    'EMAIL_SHORT': f'info@stroimarcet.ru',
    'ADDRESS_FULL': 'ул. Большая Почтовая, 26',
    'ADDRESS_SHORT': 'ул. Большая Почтовая, 26',
    'MAIN_MENU_ITEMS': MENU,
    "FORM": ContactForm()
}


def global_settings(request):
    return DATA_LIB
