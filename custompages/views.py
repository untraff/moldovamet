from django.shortcuts import render
from django.views import generic

from custompages.models import Page


class PageDetailView(generic.DetailView):
    model = Page
    
    def get_context_data(self, **kwargs):
        context = super(PageDetailView, self).get_context_data(**kwargs)
        context['h1'] = self.object.title
        context['title'] = self.object.title
        return context
