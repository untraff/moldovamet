from pprint import pprint
import json
from copy import copy
from django.conf import settings

from core.config import COMPANY_NAME


class Schema:

    def __init__(self,
                 name: str,
                 url: str,
                 hightes_price=0,
                 cheapest_price=0,
                 offer_urls=None,
                 image=None,
                 description='',
                 sku=''
                 ):
        self.name = name
        self.url = url
        self.hightes_price = hightes_price
        self.cheapest_price = cheapest_price
        self.offer_urls = copy(offer_urls)
        self.image = image
        self.description = description
        self.sku = sku

        self.offers = []
        if offer_urls:
            for offer_url in offer_urls:
                offer = {'@type': 'Offer', 'url': offer_url}
                self.offers.append(offer)

        if image is not None:
            self.image = image

    def get_aggregate_offers(self):
        return {
            '@context': 'https://schema.org/',
            '@type': 'Product',
            'brand': {'@type': 'Brand',
                      'name': COMPANY_NAME},
            'description': self.description,
            'aggregateRating': {
                '@type': 'aggregateRating',
                'ratingValue': 4.5,
                'bestRating': 5,
                'worstRating': 1,
                'ratingCount': 4,
                'reviewCount': 1
            },
            'image': self.image,
            'name': self.name,
            'offers': {'@type': 'AggregateOffer',
                       'highPrice': self.hightes_price,
                       'lowPrice': self.cheapest_price,
                       'offerCount': len(self.offer_urls),
                       'offers': self.offers,
                       'priceCurrency': 'RUB',
                       'url': self.url},
            'sku': self.sku
        }

    def get_offer(self):
        return {
            "@context": "https://schema.org/",
            "@type": "Product",
            "name": self.name,
            "image": [
                self.image,
            ],
            "description": self.description,
            # "sku": "0446310786",
            # "mpn": "925872",
            "brand": {
                "@type": "Brand",
                "name": COMPANY_NAME
            },
            # "review": {
            #     "@type": "Review",
            #     "reviewRating": {
            #         "@type": "Rating",
            #         "ratingValue": "4",
            #         "bestRating": "5"
            #     },
            #     "author": {
            #         "@type": "Person",
            #         "name": "Fred Benson"
            #     }
            # },
            # "aggregateRating": {
            #     "@type": "AggregateRating",
            #     "ratingValue": "4.4",
            #     "reviewCount": "89"
            # },
            "offers": {
                "@type": "Offer",
                "url": self.url,
                "priceCurrency": "RUB",
                "price": "10",
                "priceValidUntil": "2020-11-20",
                "itemCondition": "https://schema.org/UsedCondition",
                "availability": "https://schema.org/InStock",
                "seller": {
                    "@type": "Organization",
                    "name": "Металл"
                }
            }
        }


class SchemaBreadCrumb:

    def __init__(self):
        self.schema = {
            '@context': 'http://schema.org',
            '@type': 'BreadcrumbList',
            'itemListElement': [],
        }
        self.counter = 0

    def add_breadcrumb(self, url: str, name: str):
        block = {
           '@type': 'ListItem',
           'position': self.counter,
           'item':
           {
               '@id': url,
               'name': name
           }
        }
        self.schema['itemListElement'].append(block)
        self.counter += 1

    def get_json_schema(self):
        return json.dumps(self.schema, ensure_ascii=False)
